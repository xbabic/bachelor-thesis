#include <app/examples.hpp>
#include <com/frame.hpp>
#include <gfx/index.hpp>
#include <gfx/renderer.hpp>
#include <gfx/text_shader.hpp>
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/color_space.hpp>
#include <iostream>
#include <random>
#include <app/program_options.hpp>
#include <osi/window.hpp>

#define BENCHMARK_BOX_HALF_SIZE 30.f
#define BENCHMARK_BOX_OBJECT_HALF_SIZE 2.5f
#define LIGHT_TO_BOX_RATIO 2

namespace app {

namespace detail {

static std::mt19937 generator;

static double random() {
    static std::uniform_real_distribution dist;
    return dist(generator);
}

static double random(double min, double max) {
    return random() * (max - min) + min;
}

static vec3 random_color() {
    return rgbColor(vec3(random(0, 360), 1.0f, 1.0f));
}

static vec3 random_position() {
    return {
        random(-BENCHMARK_BOX_HALF_SIZE, BENCHMARK_BOX_HALF_SIZE),
        random(-BENCHMARK_BOX_HALF_SIZE, BENCHMARK_BOX_HALF_SIZE),
        random(-BENCHMARK_BOX_HALF_SIZE, BENCHMARK_BOX_HALF_SIZE),
    };
}

static quat random_rotation() {
    return {{random(0, 3.14), random(0, 3.14), random(0, 3.14)}};
}

static void reset_camera() {
    gfx::camera_system()->active_frame()->set_origin({0, -120, 0});
    gfx::camera_system()->active_frame()->set_rotation({{1.57, 0., 0.}});
}

}

typedef gfx::ShaderGraph::Node::DataType::ElementType ElementType;
typedef gfx::ShaderGraph Graph;
typedef gfx::ShaderGraph::Node::SlotDefinition Slot;

void Example::activate() {
    osi::Window::set_title(name());
#ifndef MEASURING
    std::cout << "Example \"" + name() + "\" activated\n";
#endif
}

void Example::deactivate() {}

com::Folder *Example::directional_light() const {
    static Folder *directional_light = nullptr;

    if (directional_light == nullptr) {
        auto light_frame = folder()->folder()->push_back<Folder>("directional-light")->push_back<com::Frame>();
        light_frame->set_rotation(quat{ { 0.3, 0.1, 0 } });
        directional_light = gfx::light_system()->insert_light<gfx::DirectionalLight>(
            { folder_name(), "directional" }, light_frame, vec3{ 1 });
    }
    return directional_light;
}

com::Folder *Example::ambient_light() {
    static Folder *ambient_light = nullptr;

    if (ambient_light == nullptr)
        ambient_light = gfx::light_system()->insert_light<gfx::AmbientLight>(
            { folder_name(), "ambient" }, nullptr, vec3{ 1 });
    return ambient_light;
}

com::Folder *Example::generate_point_light(const std::string &light_name, vec3 position, vec3 color, float atten_const,
                                           float atten_lin, float atten_quad) {
    auto point_light_frame = push_back<Folder>(light_name)->push_back<com::Frame>();
    point_light_frame->move_origin(position);
    auto point_light = gfx::light_system()->insert_light<gfx::PointLight>(
        { folder_name(), name(), light_name }, point_light_frame, color, atten_const, atten_lin, atten_quad);

    auto material =
        gfx::material_system()->insert_default_material(light_name + "-material", { folder_name(), name() }, color);

    // point light visualizer box
    gfx::object_system()->push_frame_back(
        gfx::object_system()->insert_object(
            { folder_name(), name(), light_boxes_folder_name(), light_name + "-sphere" }, material,
            gfx::buffer_generators()->insert_procedural_sphere_solid(0.05f, 6, light_name + "-sphere",
                                                                     { folder_name(), name() })),
        point_light_frame);

    return point_light;
}

com::Folder *Example::random_point_light() {
    static unsigned i = 0;

    auto position = detail::random_position();
    auto light_name = "random-light-" + std::to_string(i++);

    auto point_light_frame = push_back<Folder>(light_name)->push_back<com::Frame>();
    point_light_frame->move_origin(position);

    auto point_light = gfx::light_system()->insert_light<gfx::PointLight>(
        { folder_name(), name(), light_name }, point_light_frame, detail::random_color(), 0.2f, 0.0f, 1.0f);

    return point_light;
}

void Example::add_random_frame(Folder *object) {
    static unsigned i = 0;
    auto frame = push_back<Folder>("random-frame-" + std::to_string(i++))->push_back<com::Frame>();
    frame->move_origin(detail::random_position());
    frame->set_rotation(detail::random_rotation());
    gfx::object_system()->push_frame_back(object, frame);
}

void DefaultTextShader::initialize() {
    static const std::string default_vertex_shader = R"(
#version 330 core
layout(location = 0) in vec3 vertex_position;
uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;
void main() {
    gl_Position = projection * view * model * vec4(vertex_position, 1.);
})";

    static const std::string default_fragment_shader = R"(
#version 330 core
out vec4 fragColor;
void main() {
    fragColor = vec4(0.2, 0.8, 1., 1.);
})";

    auto material = gfx::material_system()->insert_material(
        "material",
        gfx::shader_system()->insert_shader<gfx::TextShader>(
            { folder_name(), name() },
            "shader",
            gfx::Shader::UNLIT,
            default_vertex_shader, default_fragment_shader),
        { folder_name(), name() });

    auto box = gfx::object_system()->insert_object(
        { folder_name(), name(), "box" },
        material,
        gfx::buffer_generators()->insert_procedural_box_solid(
            vec3{ 0.5 },
            "box",
            { folder_name(), name() }));

    gfx::object_system()->push_frame_back(box, push_back<com::Frame>());

    m_objects = box;
}

void DefaultTextShader::next_round(Folder *grid) {
    gfx::renderer()->clear_render_buffers();
    gfx::renderer()->present_collection(m_objects, gfx::Renderer::FORWARD);
    gfx::renderer()->present_collection(grid, gfx::Renderer::FORWARD);
}

void QuadTextShader::initialize() {
    static const std::string rectangle_vertex_shader = R"(
#version 330 core
layout(location = 0) in vec3 vertex_position;
layout(location = 3) in vec2 a_tex_coords;
out vec2 tex_coords;
void main() {
    tex_coords = a_tex_coords;
    gl_Position = vec4(vertex_position, 1.);
})";

    static const std::string rectangle_fragment_shader = R"(
#version 330 core
in vec2 tex_coords;
out vec4 fragColor;
void main() {
    fragColor = vec4(tex_coords, 1., 1.);
})";

    auto shader = gfx::shader_system()->insert_shader<gfx::TextShader>(
        { folder_name(), name() },
        "shader",
        gfx::Shader::UNLIT,
        rectangle_vertex_shader,
        rectangle_fragment_shader);

    auto material = gfx::material_system()->insert_material(
        "material",
        shader,
        { folder_name(), name() });

    auto rect = gfx::object_system()->insert_object(
        { folder_name(), name() },
        material,
        gfx::buffer_generators()->insert_procedural_rectangle(
            { 1, 1 },
            "rectangle",
            { folder_name(), name() }));

    gfx::object_system()->push_frame_back(rect, push_back<com::Frame>());

    m_objects = rect;
}

void QuadTextShader::next_round(Folder *grid) {
    gfx::renderer()->clear_render_buffers();
    gfx::renderer()->present_collection(m_objects, gfx::Renderer::FORWARD);
    gfx::renderer()->present_collection(grid, gfx::Renderer::FORWARD);
}

void ForwardLit::initialize() {
    auto shader = gfx::shader_system()->insert_shader<Graph>(
            { folder_name(), name() },
            "box-shader",
            Graph::LIT);

    auto normal = shader->insert<Graph::ConstantNode>(vec3{0.5, 0.5, 1.});
    shader->connect(shader->root(), Graph::MasterNodeLit::inputs::normal_ts, normal, 0);

    auto container = shader->insert<Graph::TextureNode>(
        com::ContextPath{ "age", "texture", "container2.png" }, ElementType::VEC3);
    auto specular = shader->insert<Graph::TextureNode>(
        com::ContextPath{ "age", "texture", "container2_specular.png" }, ElementType::VEC3);

    auto tex_coords = shader->insert<Graph::VaryingTexcoordNode>();
    shader->connect(container, Graph::TextureNode::inputs::tex_coord, tex_coords, 0);
    shader->connect(specular, Graph::TextureNode::inputs::tex_coord, tex_coords, 0);

    shader->connect(shader->root(), Graph::MasterNodeLit::inputs::ambient, container, 0);
    shader->connect(shader->root(), Graph::MasterNodeLit::inputs::diffuse, container, 0);
    shader->connect(shader->root(), Graph::MasterNodeLit::inputs::specular, specular, 0);

    auto emission = shader->insert<Graph::ConstantNode>(vec3{0.});
    shader->connect(shader->root(), Graph::MasterNodeLit::inputs::emission, emission, 0);

    auto material = gfx::material_system()->insert_material(
        "box-material",
        shader,
        { folder_name(), name() });

    auto box = gfx::object_system()->insert_object(
        { folder_name(), name(), "box" },
        material,
        gfx::buffer_generators()->insert_procedural_box_solid(
            vec3{0.5},
            "box",
            { folder_name(), name() }));

    m_objects = box->folder();

    gfx::object_system()->push_frame_back(box, push_back<com::Frame>());

    gfx::object_system()->insert_light(box, ambient_light());
    gfx::object_system()->insert_light(box, directional_light());

    std::vector point_lights{
        generate_point_light("point-light-1", { 1, 1, 1 }, { 0, 1, 1 }, 0.1f, 0.0f, 1.0f),
        generate_point_light("point-light-2", { -1, -1.5, -1 }, { 1, 0, 1 }, 0.1f, 0.0f, 1.0f),
        generate_point_light("point-light-3", { 0.25, 1.25, -0.25 }, { 1, 1, 0 }, 0.1f, 0.0f, 1.0f),
    };
    for (auto &pl : point_lights)
        gfx::object_system()->insert_light(box, pl);
}

void ForwardLit::next_round(Folder *grid) {
    gfx::renderer()->clear_render_buffers();
    gfx::renderer()->present_collection(m_objects, gfx::Renderer::FORWARD);
    gfx::renderer()->present_collection(grid, gfx::Renderer::FORWARD);
}

void DeferredShading::initialize() {
    auto shader = gfx::shader_system()->insert_shader<Graph>(
            { folder_name(), name() },
            "box-shader",
            Graph::DEFERRED);

    auto normal = shader->insert<Graph::ConstantNode>(vec3{0.5, 0.5, 1.});
    shader->connect(shader->root(), Graph::MasterNodeDeferred::inputs::normal_ts, normal, 0);

    auto container = shader->insert<Graph::TextureNode>(
        com::ContextPath{ "age", "texture", "container2.png" }, ElementType::VEC3);
    auto specular = shader->insert<Graph::TextureNode>(
        com::ContextPath{ "age", "texture", "container2_specular.png" }, ElementType::VEC3);

    auto tex_coords = shader->insert<Graph::VaryingTexcoordNode>();
    shader->connect(container, Graph::TextureNode::inputs::tex_coord, tex_coords, 0);
    shader->connect(specular, Graph::TextureNode::inputs::tex_coord, tex_coords, 0);

    shader->connect(shader->root(), Graph::MasterNodeDeferred::inputs::ambient, container, 0);
    shader->connect(shader->root(), Graph::MasterNodeDeferred::inputs::diffuse, container, 0);
    shader->connect(shader->root(), Graph::MasterNodeDeferred::inputs::specular, specular, 0);

    auto emission = shader->insert<Graph::ConstantNode>(vec3{0.});
    shader->connect(shader->root(), Graph::MasterNodeDeferred::inputs::emission, emission, 0);

    auto material = gfx::material_system()->insert_material(
        "box-material",
        shader,
        { folder_name(), name() });

    auto box = gfx::object_system()->insert_object(
        { folder_name(), name(), "deferred", "box" },
        material,
        gfx::buffer_generators()->insert_procedural_box_solid(
            vec3{0.5},
            "box",
            { folder_name(), name() }));

    m_objects = box->folder()->folder();

    gfx::object_system()->push_frame_back(box, push_back<com::Frame>());

    gfx::object_system()->insert_light(box, ambient_light());
    gfx::object_system()->insert_light(box, directional_light());

    std::vector point_lights{
        generate_point_light("point-light-1", { 1, 1, 1 }, { 0, 1, 1 }, 0.1f, 0.0f, 1.0f),
        generate_point_light("point-light-2", { -1, -1.5, -1 }, { 1, 0, 1 }, 0.1f, 0.0f, 1.0f),
        generate_point_light("point-light-3", { 0.25, 1.25, -0.25 }, { 1, 1, 0 }, 0.1f, 0.0f, 1.0f),
    };
    for (auto &pl : point_lights)
        gfx::object_system()->insert_light(box, pl);

    gfx::Renderer::gather_lights(box, m_gathered_lights);
}

void DeferredShading::next_round(Folder *grid) {
    gfx::renderer()->clear_render_buffers(true);

    auto deferred = m_objects->find<Folder>("deferred");
    gfx::renderer()->present_collection(deferred, gfx::Renderer::DEFERRED);
    gfx::renderer()->lighting_pass(m_gathered_lights);

    gfx::renderer()->copy_depth_buffer();

    auto lights_folder = m_objects->find<Folder>(light_boxes_folder_name());
    gfx::renderer()->present_collection(lights_folder, gfx::Renderer::FORWARD);
    gfx::renderer()->present_collection(grid, gfx::Renderer::FORWARD);
}

void AlphaBlend::initialize() {
    auto shader = gfx::shader_system()->insert_shader<Graph>(
            { folder_name(), name() },
            "box-shader",
            gfx::Shader::LIT,
            Graph::PHONG,
            Graph::PARTIAL);

    auto normal = shader->insert<Graph::ConstantNode>(vec3{0.5, 0.5, 1.});
    shader->connect(shader->root(), Graph::MasterNodeLit::inputs::normal_ts, normal, 0);

    auto container = shader->insert<Graph::TextureNode>(
        com::ContextPath{ "age", "texture", "container2.png" }, ElementType::VEC3);
    auto specular = shader->insert<Graph::TextureNode>(
        com::ContextPath{ "age", "texture", "container2_specular.png" }, ElementType::VEC3);

    auto tex_coords = shader->insert<Graph::VaryingTexcoordNode>();
    shader->connect(container, Graph::TextureNode::inputs::tex_coord, tex_coords, 0);
    shader->connect(specular, Graph::TextureNode::inputs::tex_coord, tex_coords, 0);

    shader->connect(shader->root(), Graph::MasterNodeLit::inputs::ambient, container, 0);
    shader->connect(shader->root(), Graph::MasterNodeLit::inputs::diffuse, container, 0);
    shader->connect(shader->root(), Graph::MasterNodeLit::inputs::specular, specular, 0);

    auto alpha = shader->insert<Graph::ConstantNode>(0.5f);
    shader->connect(shader->root(), Graph::MasterNodeLit::inputs::alpha, alpha, 0);

    auto emission = shader->insert<Graph::ConstantNode>(vec3{0.});
    shader->connect(shader->root(), Graph::MasterNodeLit::inputs::emission, emission, 0);

    auto material = gfx::material_system()->insert_material(
            "box-material",
            shader,
            { folder_name(), name() });

    auto box = gfx::object_system()->insert_object(
        { folder_name(), name(), "transparent", "box" },
        material,
        gfx::buffer_generators()->insert_procedural_box_solid(
            vec3{0.5},
            "box",
            { folder_name(), name() }));

    m_objects = box->folder()->folder();

    auto frame = push_back<Folder>("frame-1")->push_back<com::Frame>();
    gfx::object_system()->push_frame_back(box, frame);
    frame = push_back<Folder>("frame-2")->push_back<com::Frame>();
    frame->move_origin(vec3{-1, 1, 0.5});
    gfx::object_system()->push_frame_back(box, frame);
    frame = push_back<Folder>("frame-3")->push_back<com::Frame>();
    frame->move_origin(vec3{2, 0, 1});
    gfx::object_system()->push_frame_back(box, frame);

    gfx::object_system()->insert_light(box, ambient_light());
    gfx::object_system()->insert_light(box, directional_light());

    std::vector point_lights{
        generate_point_light("point-light-1", { 1, 1, 1 }, { 0, 1, 1 }, 0.1f, 0.0f, 1.0f),
        generate_point_light("point-light-2", { -1, -1.5, -1 }, { 1, 0, 1 }, 0.1f, 0.0f, 1.0f),
        generate_point_light("point-light-3", { 0.25, 1.25, -0.25 }, { 1, 1, 0 }, 0.1f, 0.0f, 1.0f),
    };
    for (auto &pl : point_lights)
        gfx::object_system()->insert_light(box, pl);
}

void AlphaBlend::next_round(Folder *grid) {
    gfx::renderer()->clear_render_buffers();
    auto lights_folder = m_objects->find<Folder>(light_boxes_folder_name());
    gfx::renderer()->present_collection(lights_folder, gfx::Renderer::FORWARD);
    gfx::renderer()->present_collection(grid, gfx::Renderer::FORWARD);

    auto transparent = m_objects->find<Folder>("transparent");
    gfx::renderer()->present_collection(transparent, gfx::Renderer::TRANSPARENT);
}

void AlphaThreshold::initialize() {
    auto shader = gfx::shader_system()->insert_shader<Graph>(
            { folder_name(), name() },
            "box-shader",
            gfx::Shader::LIT,
            Graph::PHONG,
            Graph::THRESHOLD);

    auto normal = shader->insert<Graph::ConstantNode>(vec3{0.5, 0.5, 1.});
    shader->connect(shader->root(), Graph::MasterNodeLit::inputs::normal_ts, normal, 0);

    auto tex_coords = shader->insert<Graph::VaryingTexcoordNode>();
    auto specular = shader->insert<Graph::TextureNode>(
        com::ContextPath{ "age", "texture", "container2_specular.png" }, ElementType::VEC3);
    shader->connect(specular, Graph::TextureNode::inputs::tex_coord, tex_coords, 0);

    shader->connect(shader->root(), Graph::MasterNodeLit::inputs::ambient, specular, 0);
    shader->connect(shader->root(), Graph::MasterNodeLit::inputs::diffuse, specular, 0);
    shader->connect(shader->root(), Graph::MasterNodeLit::inputs::specular, specular, 0);

    auto decomp = shader->insert<Graph::DecomposeVectorNode>(Graph::Node::DataType::VEC3);
    shader->connect(decomp, 0, specular, 0);

    shader->connect(shader->root(), Graph::MasterNodeLit::inputs::alpha, decomp, 0);

    auto thresh = shader->insert<Graph::ConstantNode>(0.01f);
    shader->connect(shader->root(), Graph::MasterNodeLit::inputs::alpha_threshold, thresh, 0);

    auto emission = shader->insert<Graph::ConstantNode>(vec3{0.});
    shader->connect(shader->root(), Graph::MasterNodeLit::inputs::emission, emission, 0);

    auto material = gfx::material_system()->insert_material(
            "box-material",
            shader,
            { folder_name(), name() });

    auto box = gfx::object_system()->insert_object(
        { folder_name(), name(), "box" },
        material,
        gfx::buffer_generators()->insert_procedural_box_solid(
            vec3{0.5},
            "box",
            { folder_name(), name() }));

    m_objects = box->folder();

    gfx::object_system()->push_frame_back(box, push_back<com::Frame>());

    gfx::object_system()->insert_light(box, ambient_light());
    gfx::object_system()->insert_light(box, directional_light());

    std::vector point_lights{
        generate_point_light("point-light-1", { 1, 1, 1 }, { 0, 1, 1 }, 0.1f, 0.0f, 1.0f),
        generate_point_light("point-light-2", { -1, -1.5, -1 }, { 1, 0, 1 }, 0.1f, 0.0f, 1.0f),
        generate_point_light("point-light-3", { 0.25, 1.25, -0.25 }, { 1, 1, 0 }, 0.1f, 0.0f, 1.0f),
    };
    for (auto &pl : point_lights)
        gfx::object_system()->insert_light(box, pl);
}

void AlphaThreshold::next_round(Folder *grid) {
    gfx::renderer()->clear_render_buffers();
    gfx::renderer()->present_collection(m_objects, gfx::Renderer::FORWARD);
    gfx::renderer()->present_collection(grid, gfx::Renderer::FORWARD);
}

void AlphaThreshold::activate() {
    Example::activate();
    gfx::Renderer::configure_face_culling(false);
}

void AlphaThreshold::deactivate() {
    gfx::Renderer::configure_face_culling(true);
}

void Animation::initialize() {
    auto shader = gfx::shader_system()->insert_shader<Graph>(
            {folder_name(), name()},
            "shader",
            Graph::LIT);

    auto normal = shader->insert<Graph::ConstantNode>(vec3{0.5, 0.5, 1.});
    shader->connect(shader->root(), Graph::MasterNodeLit::inputs::normal_ts, normal, 0);

    auto color = shader->insert<Graph::ConstantNode>(vec3{1., 0.6, 0.2});
    shader->connect(shader->root(), Graph::MasterNodeLit::inputs::ambient, color, 0);
    shader->connect(shader->root(), Graph::MasterNodeLit::inputs::diffuse, color, 0);
    shader->connect(shader->root(), Graph::MasterNodeLit::inputs::specular, color, 0);

    auto position = shader->insert<Graph::VaryingPositionNode>();
    auto time = shader->insert<Graph::TimeNode>(0.002f);
    auto animate = shader->insert<Graph::CustomFunctionNode>(
        std::vector<Slot>{
            {"position", "position", {ElementType::VEC3}},
            {"time", "time", {ElementType::SCALAR}},
        },
        Slot{"animate", "animate", {ElementType::VEC3}},
        "    return vec3(position.x + cos(time), position.y + sin(time), position.z);");

    shader->connect(animate, 0, position, 0);
    shader->connect(animate, 1, time, 0);
    shader->disconnect(shader->root(), Graph::MasterNodeLit::inputs::position);
    shader->connect(shader->root(), Graph::MasterNodeLit::inputs::position, animate, 0);

    auto emission = shader->insert<Graph::ConstantNode>(vec3{0.});
    shader->connect(shader->root(), Graph::MasterNodeLit::inputs::emission, emission, 0);

    auto material = gfx::material_system()->insert_material(
        "material",
        shader,
        { folder_name(), name() });

    auto sphere = gfx::object_system()->insert_object(
        { folder_name(), name(), "sphere" },
        material,
        gfx::buffer_generators()->insert_procedural_sphere_solid(
            0.5, 10, "sphere", { folder_name(), name() }));

    gfx::object_system()->push_frame_back(sphere, push_back<com::Frame>());

    m_objects = sphere;

    gfx::object_system()->insert_light(sphere, directional_light());
}

void Animation::next_round(Folder *grid) {
    gfx::renderer()->clear_render_buffers();
    gfx::renderer()->present_collection(m_objects, gfx::Renderer::FORWARD);
    gfx::renderer()->present_collection(grid, gfx::Renderer::FORWARD);
}

void NormalMap::initialize() {
    auto shader = gfx::shader_system()->insert_shader<Graph>(
            { folder_name(), name() },
            "box-shader",
            gfx::Shader::LIT);

    auto tex_coords = shader->insert<Graph::VaryingTexcoordNode>();

    auto normal = shader->insert<Graph::TextureNode>(
        com::ContextPath{ "age", "texture", "brickwall_normal.jpg" }, ElementType::VEC3);
    shader->connect(normal, Graph::TextureNode::inputs::tex_coord, tex_coords, 0);
    shader->connect(shader->root(), Graph::MasterNodeLit::inputs::normal_ts, normal, 0);

    auto wall = shader->insert<Graph::TextureNode>(
        com::ContextPath{ "age", "texture", "brickwall.jpg" }, ElementType::VEC3);
    shader->connect(wall, Graph::TextureNode::inputs::tex_coord, tex_coords, 0);

    shader->connect(shader->root(), Graph::MasterNodeLit::inputs::ambient, wall, 0);
    shader->connect(shader->root(), Graph::MasterNodeLit::inputs::diffuse, wall, 0);
    shader->connect(shader->root(), Graph::MasterNodeLit::inputs::specular, wall, 0);

    auto emission = shader->insert<Graph::ConstantNode>(vec3{0.});
    shader->connect(shader->root(), Graph::MasterNodeLit::inputs::emission, emission, 0);

    auto material = gfx::material_system()->insert_material(
            "box-material",
            shader,
            { folder_name(), name() });

    auto box = gfx::object_system()->insert_object(
        { folder_name(), name(), "box" },
        material,
        gfx::buffer_generators()->insert_procedural_box_solid(
            vec3{0.5},
            "box",
            { folder_name(), name() }));

    m_objects = box->folder();

    gfx::object_system()->push_frame_back(box, push_back<com::Frame>());

    gfx::object_system()->insert_light(box, ambient_light());

    std::vector point_lights{
        generate_point_light("point-light-1", { 1, 1, 1 }, { 0, 1, 1 }, 0.1f, 0.0f, 1.0f),
        generate_point_light("point-light-2", { -1, -1.5, -1 }, { 1, 0, 1 }, 0.1f, 0.0f, 1.0f),
        generate_point_light("point-light-3", { 0.25, 1.25, -0.25 }, { 1, 1, 0 }, 0.1f, 0.0f, 1.0f),
    };
    for (auto &pl : point_lights)
        gfx::object_system()->insert_light(box, pl);
}

void NormalMap::next_round(Folder *grid) {
    gfx::renderer()->clear_render_buffers();
    gfx::renderer()->present_collection(m_objects, gfx::Renderer::FORWARD);
    gfx::renderer()->present_collection(grid, gfx::Renderer::FORWARD);
}

void PBR::initialize() {
    auto shader = gfx::shader_system()->insert_shader<Graph>(
            { folder_name(), name() },
            "box-shader",
            Graph::LIT,
            Graph::PBR);

    auto tex_coords = shader->insert<Graph::VaryingTexcoordNode>();

    auto normal = shader->insert<Graph::TextureNode>(
        com::ContextPath{ "age", "texture", "rusted_iron", "normal.png" }, ElementType::VEC3);
    shader->connect(normal, 0, tex_coords, 0);
    shader->connect(shader->root(), Graph::MasterNodePBR::inputs::normal_ts, normal, 0);

    auto albedo = shader->insert<Graph::TextureNode>(
        com::ContextPath{ "age", "texture", "rusted_iron", "albedo.png" }, ElementType::VEC3);
    shader->connect(albedo, 0, tex_coords, 0);
    shader->connect(shader->root(), Graph::MasterNodePBR::inputs::albedo, albedo, 0);

    auto metallic = shader->insert<Graph::TextureNode>(
        com::ContextPath{ "age", "texture", "rusted_iron", "metallic.png" }, ElementType::SCALAR);
    shader->connect(metallic, 0, tex_coords, 0);
    shader->connect(shader->root(), Graph::MasterNodePBR::inputs::metallic, metallic, 0);

    auto roughness = shader->insert<Graph::TextureNode>(
        com::ContextPath{ "age", "texture", "rusted_iron", "roughness.png" }, ElementType::SCALAR);
    shader->connect(roughness, 0, tex_coords, 0);
    shader->connect(shader->root(), Graph::MasterNodePBR::inputs::roughness, roughness, 0);

    auto ao = shader->insert<Graph::TextureNode>(
        com::ContextPath{ "age", "texture", "rusted_iron", "ao.png" }, ElementType::SCALAR);
    shader->connect(ao, 0, tex_coords, 0);
    shader->connect(shader->root(), Graph::MasterNodePBR::inputs::ao, ao, 0);

    auto emission = shader->insert<Graph::ConstantNode>(vec3{0.});
    shader->connect(shader->root(), Graph::MasterNodePBR::inputs::emission, emission, 0);

    auto material = gfx::material_system()->insert_material(
            "box-material",
            shader,
            { folder_name(), name() });

    auto box = gfx::object_system()->insert_object(
        { folder_name(), name(), "box" },
        material,
        gfx::buffer_generators()->insert_procedural_box_solid(
            vec3{0.5},
            "box",
            { folder_name(), name() }));

    m_objects = box->folder();

    gfx::object_system()->push_frame_back(box, push_back<com::Frame>());

    std::vector point_lights{
        generate_point_light("point-light-1", { 1, 1, 1 }, { 0, 1, 1 }, 0.1f, 0.0f, 1.0f),
        generate_point_light("point-light-2", { -1, -1.5, -1 }, { 1, 0, 1 }, 0.1f, 0.0f, 1.0f),
        generate_point_light("point-light-3", { 0.25, 1.25, -0.25 }, { 1, 1, 0 }, 0.1f, 0.0f, 1.0f),
    };
    for (auto &pl : point_lights)
        gfx::object_system()->insert_light(box, pl);
}

void PBR::next_round(Folder *grid) {
    gfx::renderer()->clear_render_buffers();
    gfx::renderer()->present_collection(m_objects, gfx::Renderer::FORWARD);
    gfx::renderer()->present_collection(grid, gfx::Renderer::FORWARD);
}

void DeferredPBR::initialize() {
    auto shader = gfx::shader_system()->insert_shader<Graph>(
            { folder_name(), name() },
            "box-shader",
            Graph::DEFERRED,
            Graph::PBR);

    auto tex_coords = shader->insert<Graph::VaryingTexcoordNode>();

    auto normal = shader->insert<Graph::TextureNode>(
        com::ContextPath{ "age", "texture", "rusted_iron", "normal.png" }, ElementType::VEC3);
    shader->connect(normal, 0, tex_coords, 0);
    shader->connect(shader->root(), Graph::MasterNodeDeferredPBR::inputs::normal_ts, normal, 0);

    auto albedo = shader->insert<Graph::TextureNode>(
        com::ContextPath{ "age", "texture", "rusted_iron", "albedo.png" }, ElementType::VEC3);
    shader->connect(albedo, 0, tex_coords, 0);
    shader->connect(shader->root(), Graph::MasterNodeDeferredPBR::inputs::albedo, albedo, 0);

    auto metallic = shader->insert<Graph::TextureNode>(
        com::ContextPath{ "age", "texture", "rusted_iron", "metallic.png" }, ElementType::SCALAR);
    shader->connect(metallic, 0, tex_coords, 0);
    shader->connect(shader->root(), Graph::MasterNodeDeferredPBR::inputs::metallic, metallic, 0);

    auto roughness = shader->insert<Graph::TextureNode>(
        com::ContextPath{ "age", "texture", "rusted_iron", "roughness.png" }, ElementType::SCALAR);
    shader->connect(roughness, 0, tex_coords, 0);
    shader->connect(shader->root(), Graph::MasterNodeDeferredPBR::inputs::roughness, roughness, 0);

    auto ao = shader->insert<Graph::TextureNode>(
        com::ContextPath{ "age", "texture", "rusted_iron", "ao.png" }, ElementType::SCALAR);
    shader->connect(ao, 0, tex_coords, 0);
    shader->connect(shader->root(), Graph::MasterNodeDeferredPBR::inputs::ao, ao, 0);

    auto emission = shader->insert<Graph::ConstantNode>(vec3{0.});
    shader->connect(shader->root(), Graph::MasterNodeDeferredPBR::inputs::emission, emission, 0);

    auto material = gfx::material_system()->insert_material(
            "box-material",
            shader,
            { folder_name(), name() });

    auto box = gfx::object_system()->insert_object(
            { folder_name(), name(), "deferred", "box" },
            material,
            gfx::buffer_generators()->insert_procedural_box_solid(
                vec3{0.5},
                "box",
                { folder_name(), name() }));

    m_objects = box->folder()->folder();

    gfx::object_system()->push_frame_back(box, push_back<com::Frame>());

    std::vector point_lights{
        generate_point_light("point-light-1", { 1, 1, 1 }, { 0, 1, 1 }, 0.1f, 0.0f, 1.0f),
        generate_point_light("point-light-2", { -1, -1.5, -1 }, { 1, 0, 1 }, 0.1f, 0.0f, 1.0f),
        generate_point_light("point-light-3", { 0.25, 1.25, -0.25 }, { 1, 1, 0 }, 0.1f, 0.0f, 1.0f),
    };
    for (auto &pl : point_lights)
        gfx::object_system()->insert_light(box, pl);

    gfx::Renderer::gather_lights(box, m_gathered_lights);
}

void DeferredPBR::next_round(Folder *grid) {
    gfx::renderer()->clear_render_buffers(true);

    auto deferred = m_objects->find<Folder>("deferred");
    gfx::renderer()->present_collection(deferred, gfx::Renderer::DEFERRED);
    gfx::renderer()->lighting_pass(m_gathered_lights, Graph::PBR);

    gfx::renderer()->copy_depth_buffer();

    auto lights_folder = m_objects->find<Folder>(light_boxes_folder_name());
    gfx::renderer()->present_collection(lights_folder, gfx::Renderer::FORWARD);
    gfx::renderer()->present_collection(grid, gfx::Renderer::FORWARD);
}

void CustomFunction::initialize() {
    auto shader = gfx::shader_system()->insert_shader<Graph>(
                { folder_name(), name() },
                "sphere-shader",
                gfx::Shader::LIT,
                Graph::PHONG,
                Graph::THRESHOLD);

    auto normal = shader->insert<Graph::ConstantNode>(vec3{0.5, 0.5, 1.});
    shader->connect(shader->root(), Graph::MasterNodeLit::inputs::normal_ts, normal, 0);

    auto color = shader->insert<Graph::UniformNode>("color", ElementType::VEC3);
    shader->connect(shader->root(), Graph::MasterNodeLit::inputs::ambient, color, 0);
    shader->connect(shader->root(), Graph::MasterNodeLit::inputs::diffuse, color, 0);
    shader->connect(shader->root(), Graph::MasterNodeLit::inputs::specular, color, 0);

    auto thresh = shader->insert<Graph::ConstantNode>(0.25f);
    shader->connect(shader->root(), Graph::MasterNodeLit::inputs::alpha_threshold, thresh, 0);

    auto world_pos = shader->insert<Graph::WorldPositionNode>();

    auto dissolve_height = shader->insert<Graph::ConstantNode>(0.2f);
    auto wave = shader->insert<Graph::CustomFunctionNode>(
        std::vector<Slot>{
            {"edge", "edge", {ElementType::SCALAR}},
            {"pos", "pos", {ElementType::VEC3}},
        },
        Slot{"wave", "wave", {ElementType::SCALAR}},
        "    if (pos.z + sin(atan(pos.y, pos.x)*20)*0.05 < edge)\n"
        "        return 1.;\n"
        "    return 0.;"
    );
    shader->connect(wave, 0, dissolve_height, 0);
    shader->connect(wave, 1, world_pos, 0);

    shader->connect(shader->root(), Graph::MasterNodeLit::inputs::alpha, wave, 0);

    auto emission = shader->insert<Graph::ConstantNode>(vec3{0.});
    shader->connect(shader->root(), Graph::MasterNodeLit::inputs::emission, emission, 0);

    auto material = gfx::material_system()->insert_material(
        "material",
        shader,
        { folder_name(), name() });

    material->set_uniform("color", vec3{1., 0.7, 0.2});

    auto sphere = gfx::object_system()->insert_object(
        { folder_name(), name(), "sphere" },
        material,
        gfx::buffer_generators()->insert_procedural_sphere_solid(
            0.5, 10, "sphere", { folder_name(), name() }));

    gfx::object_system()->push_frame_back(sphere, push_back<com::Frame>());

    m_objects = sphere;

    gfx::object_system()->insert_light(sphere, directional_light());
}

void CustomFunction::next_round(Folder *grid) {
    gfx::renderer()->clear_render_buffers();
    gfx::renderer()->present_collection(m_objects, gfx::Renderer::FORWARD);
    gfx::renderer()->present_collection(grid, gfx::Renderer::FORWARD);
}

void CustomFunction::activate() {
    Example::activate();
    gfx::Renderer::configure_face_culling(false);
}

void CustomFunction::deactivate() {
    gfx::Renderer::configure_face_culling(true);
}

void PerlinNoise::initialize() {
    auto shader = gfx::shader_system()->insert_shader<Graph>(
                { folder_name(), name() },
                "sphere-shader",
                gfx::Shader::LIT,
                Graph::PHONG,
                Graph::THRESHOLD);

    auto normal = shader->insert<Graph::ConstantNode>(vec3{0.5, 0.5, 1.});
    shader->connect(shader->root(), Graph::MasterNodeLit::inputs::normal_ts, normal, 0);

    auto color = shader->insert<Graph::ConstantNode>(vec3{0.4f});
    shader->connect(shader->root(), Graph::MasterNodeLit::inputs::ambient, color, 0);
    shader->connect(shader->root(), Graph::MasterNodeLit::inputs::diffuse, color, 0);
    shader->connect(shader->root(), Graph::MasterNodeLit::inputs::specular, color, 0);

    auto thresh = shader->insert<Graph::ConstantNode>(0.25f);
    shader->connect(shader->root(), Graph::MasterNodeLit::inputs::alpha_threshold, thresh, 0);

    auto world_pos = shader->insert<Graph::WorldPositionNode>();
    auto wp_decomp = shader->insert<Graph::DecomposeVectorNode>(ElementType::VEC3);
    shader->connect(wp_decomp, 0, world_pos, 0);

    auto dissolve_height = shader->insert<Graph::ConstantNode>(0.2f);
    auto step = shader->insert<Graph::StepNode>();
    shader->connect(step, Graph::StepNode::edge, dissolve_height, 0);
    auto one = shader->insert<Graph::ConstantNode>(1.f);
    auto inv_step = shader->insert<Graph::SubtractNode>(ElementType::SCALAR);
    shader->connect(inv_step, 0, one, 0);
    shader->connect(inv_step, 1, step, 0);
    shader->connect(shader->root(), Graph::MasterNodeLit::inputs::alpha, inv_step, 0);

    auto wp_to_uv = shader->insert<Graph::ComposeVectorNode>(ElementType::VEC2);
    shader->connect(wp_to_uv, 0, wp_decomp, 0);
    shader->connect(wp_to_uv, 1, wp_decomp, 1);
    auto no_offset = shader->insert<Graph::ConstantNode>(vec2{0.});
    auto noise_size = shader->insert<Graph::ConstantNode>(20.f);
    auto time = shader->insert<Graph::TimeNode>(0.001f);
    auto perlin_noise = shader->insert<Graph::PerlinNoise3DNode>();
    shader->connect(perlin_noise, Graph::PerlinNoise3DNode::uv, wp_to_uv, 0);
    shader->connect(perlin_noise, Graph::PerlinNoise3DNode::offset, no_offset, 0);
    shader->connect(perlin_noise, Graph::PerlinNoise3DNode::scale, noise_size, 0);
    shader->connect(perlin_noise, Graph::PerlinNoise3DNode::time, time, 0);

    auto rescaled_noise = shader->insert<Graph::CustomFunctionNode>(
        std::vector<Slot>{
            {"noise", "noise", {ElementType::SCALAR}}
        },
        Slot{"rescale", "rescale", {ElementType::SCALAR}},
        "    return noise * 0.4 - 0.5 * 0.4;");
    shader->connect(rescaled_noise, 0, perlin_noise, 0);

    auto add_height = shader->insert<Graph::AddNode>(ElementType::SCALAR);
    shader->connect(add_height, 0, wp_decomp, 2);
    shader->connect(add_height, 1, rescaled_noise, 0);
    shader->connect(step, Graph::StepNode::x, add_height, 0);

    auto emission_thickness = shader->insert<Graph::ConstantNode>(0.1f);
    auto edge = shader->insert<Graph::SubtractNode>(ElementType::SCALAR);
    shader->connect(edge, 0, dissolve_height, 0);
    shader->connect(edge, 1, emission_thickness, 0);

    auto em_step = shader->insert<Graph::StepNode>();
    shader->connect(em_step, Graph::StepNode::edge, edge, 0);
    shader->connect(em_step, Graph::StepNode::x, add_height, 0);

    auto edge_mult = shader->insert<Graph::MultiplyNode>(ElementType::SCALAR);
    shader->connect(edge_mult, 0, em_step, 0);
    shader->connect(edge_mult, 1, inv_step, 0);

    auto emission_color = shader->insert<Graph::ConstantNode>(vec3{0., .2, 1.});
    auto emission = shader->insert<Graph::ScalarMultiplyNode>(ElementType::VEC3);
    shader->connect(emission, 0, edge_mult, 0);
    shader->connect(emission, 1, emission_color, 0);
    shader->connect(shader->root(), Graph::MasterNodeLit::inputs::emission, emission, 0);

    auto material = gfx::material_system()->insert_material(
        "material",
        shader,
        { folder_name(), name() });

    auto sphere = gfx::object_system()->insert_object(
        { folder_name(), name(), "sphere" },
        material,
        gfx::buffer_generators()->insert_procedural_sphere_solid(
            0.5, 10, "sphere", { folder_name(), name() }));

    gfx::object_system()->push_frame_back(sphere, push_back<com::Frame>());

    m_objects = sphere;

    gfx::object_system()->insert_light(sphere, directional_light());
}

void PerlinNoise::next_round(Folder *grid) {
    gfx::renderer()->clear_render_buffers();
    gfx::renderer()->present_collection(m_objects, gfx::Renderer::FORWARD);
    gfx::renderer()->present_collection(grid, gfx::Renderer::FORWARD);
}

void PerlinNoise::activate() {
    Example::activate();
    gfx::Renderer::configure_face_culling(false);
}

void PerlinNoise::deactivate() {
    gfx::Renderer::configure_face_culling(true);
}

Benchmark::Benchmark(const std::string &name, unsigned boxes, unsigned lights)
    : Example(name), m_boxes(boxes), m_lights(lights) {
    static Folder *box = nullptr;
    if (box == nullptr) {
        box = gfx::object_system()->insert_object(
            { folder_name(), "benchmark", "box" },
            gfx::material_system()->insert_default_material(
                "tmp-shader",
                { folder_name(), "benchmark" },
                vec3{1.f, 0.f, 1.f}),
            gfx::buffer_generators()->insert_procedural_box_solid(
                vec3{BENCHMARK_BOX_OBJECT_HALF_SIZE},
                "box",
                { folder_name(), "benchmark" }));

        detail::generator.seed(42);
        for (unsigned i = 0; i < m_boxes; ++i)
            add_random_frame(box);
        for (unsigned i = 0; i < m_lights; ++i)
            gfx::object_system()->insert_light(box, random_point_light());
    }

    m_objects = box;
}

void Benchmark::activate() {
    Example::activate();
    detail::reset_camera();
    ASSUMPTION(m_material != nullptr);
    gfx::object_system()->set_material(m_objects, m_material);
}

void BenchmarkForwardPhong::initialize() {
    auto shader = gfx::shader_system()->insert_shader<Graph>(
            { folder_name(), name() },
            "box-shader",
            gfx::Shader::LIT,
            Graph::PHONG);

    auto normal = shader->insert<Graph::ConstantNode>(vec3{0.5, 0.5, 1.});
    shader->connect(shader->root(), Graph::MasterNodeLit::inputs::normal_ts, normal, 0);

    auto container = shader->insert<Graph::TextureNode>(
        com::ContextPath{ "age", "texture", "container2.png" }, ElementType::VEC3);
    auto specular = shader->insert<Graph::TextureNode>(
        com::ContextPath{ "age", "texture", "container2_specular.png" }, ElementType::VEC3);

    auto tex_coords = shader->insert<Graph::VaryingTexcoordNode>();
    shader->connect(container, Graph::TextureNode::inputs::tex_coord, tex_coords, 0);
    shader->connect(specular, Graph::TextureNode::inputs::tex_coord, tex_coords, 0);

    shader->connect(shader->root(), Graph::MasterNodeLit::inputs::ambient, container, 0);
    shader->connect(shader->root(), Graph::MasterNodeLit::inputs::diffuse, container, 0);
    shader->connect(shader->root(), Graph::MasterNodeLit::inputs::specular, specular, 0);

    auto emission = shader->insert<Graph::ConstantNode>(vec3{0.});
    shader->connect(shader->root(), Graph::MasterNodeLit::inputs::emission, emission, 0);

    m_material = gfx::material_system()->insert_material(
            "box-material",
            shader,
            { folder_name(), name() });
}

void BenchmarkForwardPhong::next_round(Folder *grid) {
    gfx::renderer()->clear_render_buffers();
    gfx::renderer()->present_collection(m_objects, gfx::Renderer::FORWARD);
}

void BenchmarkForwardPhongText::initialize() {
    static const std::string vertex_shader = R"(#version 330 core

layout(location = 0) in vec3 vertex_position;
layout(location = 1) in vec3 vertex_normal;
layout(location = 3) in vec2 texture_coord;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;
uniform mat4 normal_matrix;
out vec3 position;
out vec3 normal;
out vec2 texcoord;

void main()
{
    texcoord = texture_coord;
    normal = normalize(mat3(normal_matrix) * vertex_normal);
    position = vec3(model * vec4(vertex_position, 1.));
    gl_Position = projection * view * vec4(position, 1.);
})";

    static const std::string fragment_shader = R"(#version 330 core

uniform sampler2D diffuse_texture;
uniform sampler2D specular_texture;

uniform vec3 camera_pos;

in vec3 position;
in vec3 normal;
in vec2 texcoord;

out vec4 fragColor;

struct PointLight {
    vec3 color, position;
    float atten_const, atten_lin, atten_quad, radius;
};
vec3 calc_point_light(PointLight _light, vec3 _m_diffuse, vec3 _m_specular,
                      vec3 _normal, vec3 _frag_pos, vec3 _view_dir)
{
    vec3 lightDir = normalize(_light.position - _frag_pos);
    float diff = max(dot(_normal, lightDir), 0.0);
    vec3 reflectDir = reflect(-lightDir, _normal);
    float spec = pow(max(dot(_view_dir, reflectDir), 0.0), 32);
    float distance = length(_light.position - _frag_pos);
    float attenuation = 1.0 / (_light.atten_const + _light.atten_lin * distance + _light.atten_quad * distance * distance);
    vec3 diffuse = diff * _m_diffuse;
    vec3 specular = spec * _m_specular;
    return (diffuse + specular) * attenuation * _light.color;
}

uniform int nrPointLights;
uniform PointLight pointLights[)" + std::to_string(Graph::max_point_lights) + R"(];

void main()
{
    vec3 diffuse = texture(diffuse_texture, texcoord).rgb;
    vec3 specular = texture(specular_texture, texcoord).rgb;
    vec3 viewDir = normalize(camera_pos - position);
    vec3 result = vec3(0, 0, 0);
    for (int i = 0; i < nrPointLights; i++)
        if (length(pointLights[i].position - position) <= pointLights[i].radius)
            result += calc_point_light(pointLights[i], diffuse, specular, normal, position, viewDir);
    fragColor = vec4(result, 1.);
})";

    auto shader = gfx::shader_system()->insert_shader<gfx::TextShader>(
        { folder_name(), name() },
        "box-shader",
        gfx::Shader::LIT,
        vertex_shader,
        fragment_shader);

    diffuse = std::make_unique<Graph::TextureNode>(
            com::ContextPath{ "age", "texture", "container2.png" }, ElementType::VEC3,
            "diffuse_texture");
    specular = std::make_unique<Graph::TextureNode>(
        com::ContextPath{ "age", "texture", "container2_specular.png" }, ElementType::VEC3,
        "specular_texture");

    shader->add_texture(diffuse.get());
    shader->add_texture(specular.get());

    m_material = gfx::material_system()->insert_material(
            "box-material",
            shader,
            { folder_name(), name() });
}

void BenchmarkForwardPhongText::next_round(Folder *grid) {
    gfx::renderer()->clear_render_buffers();
    gfx::renderer()->present_collection(m_objects, gfx::Renderer::FORWARD);
}

void BenchmarkDeferredPhong::initialize() {
    auto shader = gfx::shader_system()->insert_shader<Graph>(
            { folder_name(), name() },
            "box-shader",
            Graph::DEFERRED);

    auto normal = shader->insert<Graph::ConstantNode>(vec3{0.5, 0.5, 1.});
    shader->connect(shader->root(), Graph::MasterNodeDeferred::inputs::normal_ts, normal, 0);

    auto container = shader->insert<Graph::TextureNode>(
        com::ContextPath{ "age", "texture", "container2.png" }, ElementType::VEC3);
    auto specular = shader->insert<Graph::TextureNode>(
        com::ContextPath{ "age", "texture", "container2_specular.png" }, ElementType::VEC3);

    auto tex_coords = shader->insert<Graph::VaryingTexcoordNode>();
    shader->connect(container, Graph::TextureNode::inputs::tex_coord, tex_coords, 0);
    shader->connect(specular, Graph::TextureNode::inputs::tex_coord, tex_coords, 0);

    shader->connect(shader->root(), Graph::MasterNodeDeferred::inputs::ambient, container, 0);
    shader->connect(shader->root(), Graph::MasterNodeDeferred::inputs::diffuse, container, 0);
    shader->connect(shader->root(), Graph::MasterNodeDeferred::inputs::specular, specular, 0);

    auto emission = shader->insert<Graph::ConstantNode>(vec3{0.});
    shader->connect(shader->root(), Graph::MasterNodeDeferred::inputs::emission, emission, 0);

    m_material = gfx::material_system()->insert_material(
        "box-material",
        shader,
        { folder_name(), name() });

    gfx::Renderer::gather_lights(m_objects, m_gathered_lights);
}

void BenchmarkDeferredPhong::next_round(Folder *grid) {
    gfx::renderer()->clear_render_buffers(true);

    gfx::renderer()->present_collection(m_objects, gfx::Renderer::DEFERRED);
    gfx::renderer()->lighting_pass(m_gathered_lights);
}

void BenchmarkDeferredPhongText::initialize() {
    static const std::string vertex_shader = R"(#version 330 core

layout(location = 0) in vec3 vertex_position;
layout(location = 1) in vec3 vertex_normal;
layout(location = 3) in vec2 texture_coord;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;
uniform mat4 normal_matrix;

out vec3 position;
out vec3 normal;
out vec2 texcoord;

void main()
{
    texcoord = texture_coord;
    normal = normalize(mat3(normal_matrix) * vertex_normal);
    position = vec3(model * vec4(vertex_position, 1.));
    gl_Position = projection * view * vec4(position, 1.);
})";

    static const std::string fragment_shader = R"(#version 330 core

uniform sampler2D diffuse_texture;
uniform sampler2D specular_texture;

in vec3 position;
in vec3 normal;
in vec2 texcoord;

layout (location = 0) out vec4 g_position;
layout (location = 1) out vec4 g_normal;
layout (location = 2) out vec4 g_ambient;
layout (location = 3) out vec4 g_diffuse;
layout (location = 4) out vec4 g_specular;

void main()
{
    vec4 diffuse = texture(diffuse_texture, texcoord);
    g_position = vec4(position, 1.);
    g_normal = vec4(normal, 0.);
    g_ambient = diffuse;
    g_diffuse = diffuse;
    g_specular = texture(specular_texture, texcoord);
})";

    auto shader = gfx::shader_system()->insert_shader<gfx::TextShader>(
        { folder_name(), name() },
        "box-shader",
        gfx::Shader::DEFERRED,
        vertex_shader,
        fragment_shader);

    diffuse = std::make_unique<Graph::TextureNode>(
            com::ContextPath{ "age", "texture", "container2.png" }, ElementType::VEC3,
            "diffuse_texture");
    specular = std::make_unique<Graph::TextureNode>(
        com::ContextPath{ "age", "texture", "container2_specular.png" }, ElementType::VEC3,
        "specular_texture");

    shader->add_texture(diffuse.get());
    shader->add_texture(specular.get());

    m_material = gfx::material_system()->insert_material(
            "box-material",
            shader,
            { folder_name(), name() });

    gfx::Renderer::gather_lights(m_objects, m_gathered_lights);
}

void BenchmarkDeferredPhongText::next_round(Folder *grid) {
    gfx::renderer()->clear_render_buffers(true);

    gfx::renderer()->present_collection(m_objects, gfx::Renderer::DEFERRED);
    gfx::renderer()->lighting_pass(m_gathered_lights);
}

void BenchmarkForwardPBR::initialize() {
    auto shader = gfx::shader_system()->insert_shader<Graph>(
            { folder_name(), name() },
            "box-shader",
            Graph::LIT,
            Graph::PBR);

    auto tex_coords = shader->insert<Graph::VaryingTexcoordNode>();

    auto normal = shader->insert<Graph::TextureNode>(
        com::ContextPath{ "age", "texture", "rusted_iron", "normal.png" }, ElementType::VEC3);
    shader->connect(normal, 0, tex_coords, 0);
    shader->connect(shader->root(), Graph::MasterNodePBR::inputs::normal_ts, normal, 0);

    auto albedo = shader->insert<Graph::TextureNode>(
        com::ContextPath{ "age", "texture", "rusted_iron", "albedo.png" }, ElementType::VEC3);
    shader->connect(albedo, 0, tex_coords, 0);
    shader->connect(shader->root(), Graph::MasterNodePBR::inputs::albedo, albedo, 0);

    auto metallic = shader->insert<Graph::TextureNode>(
        com::ContextPath{ "age", "texture", "rusted_iron", "metallic.png" }, ElementType::SCALAR);
    shader->connect(metallic, 0, tex_coords, 0);
    shader->connect(shader->root(), Graph::MasterNodePBR::inputs::metallic, metallic, 0);

    auto roughness = shader->insert<Graph::TextureNode>(
        com::ContextPath{ "age", "texture", "rusted_iron", "roughness.png" }, ElementType::SCALAR);
    shader->connect(roughness, 0, tex_coords, 0);
    shader->connect(shader->root(), Graph::MasterNodePBR::inputs::roughness, roughness, 0);

    auto ao = shader->insert<Graph::TextureNode>(
        com::ContextPath{ "age", "texture", "rusted_iron", "ao.png" }, ElementType::SCALAR);
    shader->connect(ao, 0, tex_coords, 0);
    shader->connect(shader->root(), Graph::MasterNodePBR::inputs::ao, ao, 0);

    auto emission = shader->insert<Graph::ConstantNode>(vec3{0.});
    shader->connect(shader->root(), Graph::MasterNodePBR::inputs::emission, emission, 0);

    m_material = gfx::material_system()->insert_material(
            "box-material",
            shader,
            { folder_name(), name() });
}

void BenchmarkForwardPBR::next_round(Folder *grid) {
    gfx::renderer()->clear_render_buffers();
    gfx::renderer()->present_collection(m_objects, gfx::Renderer::FORWARD);
}

void BenchmarkForwardPBRText::initialize() {
    static const std::string vertex_shader = R"(#version 330 core

layout(location = 0) in vec3 vertex_position;
layout(location = 1) in vec3 vertex_normal;
layout(location = 2) in vec3 vertex_tangent;
layout(location = 3) in vec2 texture_coord;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;
uniform mat4 normal_matrix;

out vec3 position;
out vec2 texcoord;
out mat3 TBN;

void main()
{
    texcoord = texture_coord;
    vec3 N = normalize(mat3(normal_matrix) * vertex_normal);
    vec3 T = normalize(mat3(normal_matrix) * vertex_tangent);
    vec3 B = cross(N, T);
    TBN = mat3(T, B, N);
    position = vec3(model * vec4(vertex_position, 1.));
    gl_Position = projection * view * vec4(position, 1.);
})";

    static const std::string fragment_shader = R"(#version 330 core

uniform sampler2D normal_sampler;
uniform sampler2D albedo_sampler;
uniform sampler2D metallic_sampler;
uniform sampler2D roughness_sampler;
uniform sampler2D ao_sampler;

in vec3 position;
in vec2 texcoord;
in mat3 TBN;

struct DirectionalLight {
    vec3 color, direction;
};

struct PointLight {
    vec3 color, position;
    float atten_const, atten_lin, atten_quad, radius;
};
uniform vec3 camera_pos;
out vec4 fragColor;
uniform int nrDirectionalLights;
uniform DirectionalLight directionalLights[1];
uniform int nrPointLights;
uniform PointLight pointLights[)" + std::to_string(Graph::max_point_lights) + R"(];
const float PI = 3.14159265359;
float DistributionGGX(vec3 N, vec3 H, float roughness)
{
    float a = roughness*roughness;
    float a2 = a*a;
    float NdotH = max(dot(N, H), 0.0);
    float NdotH2 = NdotH*NdotH;

    float nom   = a2;
    float denom = (NdotH2 * (a2 - 1.0) + 1.0);
    denom = PI * denom * denom;

    return nom / denom;
}
float GeometrySchlickGGX(float NdotV, float roughness)
{
    float r = (roughness + 1.0);
    float k = (r*r) / 8.0;

    float nom   = NdotV;
    float denom = NdotV * (1.0 - k) + k;

    return nom / denom;
}
float GeometrySmith(vec3 N, vec3 V, vec3 L, float roughness)
{
    float NdotV = max(dot(N, V), 0.0);
    float NdotL = max(dot(N, L), 0.0);
    float ggx2 = GeometrySchlickGGX(NdotV, roughness);
    float ggx1 = GeometrySchlickGGX(NdotL, roughness);

    return ggx1 * ggx2;
}
vec3 fresnelSchlick(float cosTheta, vec3 F0)
{
    return F0 + (1.0 - F0) * pow(clamp(1.0 - cosTheta, 0.0, 1.0), 5.0);
};

void main()
{
    vec3 normal = texture(normal_sampler, texcoord).rgb;
    vec3 albedo = texture(albedo_sampler, texcoord).rgb;
    float metallic = texture(metallic_sampler, texcoord).r;
    float roughness = texture(roughness_sampler, texcoord).r;
    float ao = texture(ao_sampler, texcoord).r;

    vec3 N = normalize(TBN * (normal * 2. - 1.));
    vec3 V = normalize(camera_pos - position);

    vec3 F0 = vec3(0.04);
    F0 = mix(F0, albedo, metallic);

    vec3 Lo = vec3(0.0);
    for(int i = 0; i < nrPointLights; ++i) {
        vec3 L = normalize(pointLights[i].position - position);
        vec3 H = normalize(V + L);
        float distance = length(pointLights[i].position - position);
        if (distance > pointLights[i].radius)
            continue;

        float attenuation = 1.0 / (pointLights[i].atten_const + pointLights[i].atten_lin * distance + pointLights[i].atten_quad * distance * distance);
        vec3 radiance = pointLights[i].color * attenuation;

        float NDF = DistributionGGX(N, H, roughness);
        float G   = GeometrySmith(N, V, L, roughness);
        vec3 F    = fresnelSchlick(max(dot(H, V), 0.0), F0);

        vec3 numerator    = NDF * G * F;
        float denominator = 4.0 * max(dot(N, V), 0.0) * max(dot(N, L), 0.0) + 0.0001; // + 0.0001 to prevent divide by zero
        vec3 specular = numerator / denominator;

        vec3 kS = F;
        vec3 kD = vec3(1.0) - kS;
        kD *= 1.0 - metallic;

        float NdotL = max(dot(N, L), 0.0);

        Lo += (kD * albedo / PI + specular) * radiance * NdotL;
    }
    for(int i = 0; i < nrDirectionalLights; ++i) {
        vec3 L = normalize(-directionalLights[i].direction);
        vec3 H = normalize(V + L);
        vec3 radiance = directionalLights[i].color;
        float NDF = DistributionGGX(N, H, roughness);
        float G   = GeometrySmith(N, V, L, roughness);
        vec3 F    = fresnelSchlick(max(dot(H, V), 0.0), F0);
        vec3 numerator    = NDF * G * F;
        float denominator = 4.0 * max(dot(N, V), 0.0) * max(dot(N, L), 0.0) + 0.0001;
        vec3 specular = numerator / denominator;
        vec3 kS = F;
        vec3 kD = vec3(1.0) - kS;
        kD *= 1.0 - metallic;
        float NdotL = max(dot(N, L), 0.0);
        Lo += (kD * albedo / PI + specular) * radiance * NdotL;
    }

    vec3 ambient = vec3(0.03) * albedo * ao;

    fragColor = vec4(ambient + Lo, 1.);
})";

    auto shader = gfx::shader_system()->insert_shader<gfx::TextShader>(
        { folder_name(), name() },
        "box-shader",
        gfx::Shader::LIT,
        vertex_shader,
        fragment_shader);

    m_textures.emplace_back(std::make_unique<Graph::TextureNode>(
            com::ContextPath{ "age", "texture", "rusted_iron", "normal.png" }, ElementType::VEC3,
            "normal_sampler"));

    m_textures.emplace_back(std::make_unique<Graph::TextureNode>(
        com::ContextPath{ "age", "texture", "rusted_iron", "albedo.png" }, ElementType::VEC3,
        "albedo_sampler"));

    m_textures.emplace_back(std::make_unique<Graph::TextureNode>(
        com::ContextPath{ "age", "texture", "rusted_iron", "metallic.png" }, ElementType::SCALAR,
        "metallic_sampler"));

    m_textures.emplace_back(std::make_unique<Graph::TextureNode>(
        com::ContextPath{ "age", "texture", "rusted_iron", "roughness.png" }, ElementType::SCALAR,
        "roughness_sampler"));

    m_textures.emplace_back(std::make_unique<Graph::TextureNode>(
        com::ContextPath{ "age", "texture", "rusted_iron", "ao.png" }, ElementType::SCALAR,
        "ao_sampler"));

    for (auto &t : m_textures)
        shader->add_texture(t.get());

    m_material = gfx::material_system()->insert_material(
            "box-material",
            shader,
            { folder_name(), name() });
}

void BenchmarkForwardPBRText::next_round(Folder *grid) {
    gfx::renderer()->clear_render_buffers();
    gfx::renderer()->present_collection(m_objects, gfx::Renderer::FORWARD);
}

void BenchmarkDeferredPBR::initialize() {
    auto shader = gfx::shader_system()->insert_shader<Graph>(
            { folder_name(), name() },
            "box-shader",
            Graph::DEFERRED,
            Graph::PBR);

    auto tex_coords = shader->insert<Graph::VaryingTexcoordNode>();

    auto normal = shader->insert<Graph::TextureNode>(
        com::ContextPath{ "age", "texture", "rusted_iron", "normal.png" }, ElementType::VEC3);
    shader->connect(normal, 0, tex_coords, 0);
    shader->connect(shader->root(), Graph::MasterNodeDeferredPBR::inputs::normal_ts, normal, 0);

    auto albedo = shader->insert<Graph::TextureNode>(
        com::ContextPath{ "age", "texture", "rusted_iron", "albedo.png" }, ElementType::VEC3);
    shader->connect(albedo, 0, tex_coords, 0);
    shader->connect(shader->root(), Graph::MasterNodeDeferredPBR::inputs::albedo, albedo, 0);

    auto metallic = shader->insert<Graph::TextureNode>(
        com::ContextPath{ "age", "texture", "rusted_iron", "metallic.png" }, ElementType::SCALAR);
    shader->connect(metallic, 0, tex_coords, 0);
    shader->connect(shader->root(), Graph::MasterNodeDeferredPBR::inputs::metallic, metallic, 0);

    auto roughness = shader->insert<Graph::TextureNode>(
        com::ContextPath{ "age", "texture", "rusted_iron", "roughness.png" }, ElementType::SCALAR);
    shader->connect(roughness, 0, tex_coords, 0);
    shader->connect(shader->root(), Graph::MasterNodeDeferredPBR::inputs::roughness, roughness, 0);

    auto ao = shader->insert<Graph::TextureNode>(
        com::ContextPath{ "age", "texture", "rusted_iron", "ao.png" }, ElementType::SCALAR);
    shader->connect(ao, 0, tex_coords, 0);
    shader->connect(shader->root(), Graph::MasterNodeDeferredPBR::inputs::ao, ao, 0);

    auto emission = shader->insert<Graph::ConstantNode>(vec3{0.});
    shader->connect(shader->root(), Graph::MasterNodeDeferredPBR::inputs::emission, emission, 0);

    m_material = gfx::material_system()->insert_material(
        "box-material",
        shader,
        { folder_name(), name() });

    gfx::Renderer::gather_lights(m_objects, m_gathered_lights);
}

void BenchmarkDeferredPBR::next_round(Folder *grid) {
    gfx::renderer()->clear_render_buffers(true);

    gfx::renderer()->present_collection(m_objects, gfx::Renderer::DEFERRED);
    gfx::renderer()->lighting_pass(m_gathered_lights, Graph::PBR);
}

void BenchmarkDeferredPBRText::initialize() {
    static const std::string vertex_shader = R"(#version 330 core

layout(location = 0) in vec3 vertex_position;
layout(location = 1) in vec3 vertex_normal;
layout(location = 2) in vec3 vertex_tangent;
layout(location = 3) in vec2 texture_coord;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;
uniform mat4 normal_matrix;

out vec3 position;
out vec2 texcoord;
out mat3 TBN;

void main()
{
    texcoord = texture_coord;
    vec3 N = normalize(mat3(normal_matrix) * vertex_normal);
    vec3 T = normalize(mat3(normal_matrix) * vertex_tangent);
    vec3 B = cross(N, T);
    TBN = mat3(T, B, N);
    position = vec3(model * vec4(vertex_position, 1.));
    gl_Position = projection * view * vec4(position, 1.);
})";

    static const std::string fragment_shader = R"(#version 330 core

uniform sampler2D normal_sampler;
uniform sampler2D albedo_sampler;
uniform sampler2D metallic_sampler;
uniform sampler2D roughness_sampler;
uniform sampler2D ao_sampler;

in vec3 position;
in vec2 texcoord;
in mat3 TBN;

layout (location = 0) out vec4 g_position;
layout (location = 1) out vec4 g_normal;
layout (location = 2) out vec4 g_albedo;
layout (location = 3) out vec4 g_met_rough_ao;

void main()
{
    vec3 normal = texture(normal_sampler, texcoord).rgb;
    vec3 albedo = texture(albedo_sampler, texcoord).rgb;
    float metallic = texture(metallic_sampler, texcoord).r;
    float roughness = texture(roughness_sampler, texcoord).r;
    float ao = texture(ao_sampler, texcoord).r;

    g_position = vec4(position, 1.);
    g_normal = vec4(normalize(TBN * (normal * 2. - 1.)), 0.);
    g_albedo = vec4(albedo, 1.);
    g_met_rough_ao = vec4(metallic, roughness, ao, 1.);
})";

    auto shader = gfx::shader_system()->insert_shader<gfx::TextShader>(
        { folder_name(), name() },
        "box-shader",
        gfx::Shader::DEFERRED,
        vertex_shader,
        fragment_shader);

    m_textures.emplace_back(std::make_unique<Graph::TextureNode>(
            com::ContextPath{ "age", "texture", "rusted_iron", "normal.png" }, ElementType::VEC3,
            "normal_sampler"));

    m_textures.emplace_back(std::make_unique<Graph::TextureNode>(
        com::ContextPath{ "age", "texture", "rusted_iron", "albedo.png" }, ElementType::VEC3,
        "albedo_sampler"));

    m_textures.emplace_back(std::make_unique<Graph::TextureNode>(
        com::ContextPath{ "age", "texture", "rusted_iron", "metallic.png" }, ElementType::SCALAR,
        "metallic_sampler"));

    m_textures.emplace_back(std::make_unique<Graph::TextureNode>(
        com::ContextPath{ "age", "texture", "rusted_iron", "roughness.png" }, ElementType::SCALAR,
        "roughness_sampler"));

    m_textures.emplace_back(std::make_unique<Graph::TextureNode>(
        com::ContextPath{ "age", "texture", "rusted_iron", "ao.png" }, ElementType::SCALAR,
        "ao_sampler"));

    for (auto &t : m_textures)
        shader->add_texture(t.get());

    m_material = gfx::material_system()->insert_material(
            "box-material",
            shader,
            { folder_name(), name() });

    gfx::Renderer::gather_lights(m_objects, m_gathered_lights);
}

void BenchmarkDeferredPBRText::next_round(Folder *grid) {
    gfx::renderer()->clear_render_buffers(true);

    gfx::renderer()->present_collection(m_objects, gfx::Renderer::DEFERRED);
    gfx::renderer()->lighting_pass(m_gathered_lights, Graph::PBR);
}

static void remap_node(Graph *shader) {
    Graph::Node *x = nullptr, *input_min = nullptr, *input_max = nullptr, *output_min = nullptr, *output_max = nullptr;

    auto x_minus_in_min = shader->insert<Graph::SubtractNode>(ElementType::SCALAR);
    shader->connect(x_minus_in_min, 0, x, 0);
    shader->connect(x_minus_in_min, 1, input_min, 0);
    auto in_range = shader->insert<Graph::SubtractNode>(ElementType::SCALAR);
    shader->connect(in_range, 0, input_max, 0);
    shader->connect(in_range, 1, input_min, 0);
    auto normalized_x = shader->insert<Graph::DivideNode>(ElementType::SCALAR);
    shader->connect(normalized_x, 0, x_minus_in_min, 0);
    shader->connect(normalized_x, 1, in_range, 0);
    auto out_range = shader->insert<Graph::SubtractNode>(ElementType::SCALAR);
    shader->connect(out_range, 0, output_max, 0);
    shader->connect(out_range, 1, output_min, 0);
    auto rescaled_x = shader->insert<Graph::MultiplyNode>(ElementType::SCALAR);
    shader->connect(rescaled_x, 0, normalized_x, 0);
    shader->connect(rescaled_x, 1, out_range, 0);
    auto remapped_x = shader->insert<Graph::AddNode>(ElementType::SCALAR);
    shader->connect(remapped_x, 0, rescaled_x, 0);
    shader->connect(remapped_x, 1, output_min, 0);
}

void initialize_examples(com::Folder *root) {
    auto examples = root->push_back<com::Folder>(Example::folder_name());

    auto options = get_program_options().get();
    unsigned boxes = options->has("boxes") ? options->value_as_int("boxes") : 100;
    unsigned lights = options->has("lights") ? options->value_as_int("lights") : 100;

    if (options->has("bench")) {
        unsigned bench = options->value_as_int("bench");
        switch (bench) {
        case 0:
            examples->push_back<BenchmarkForwardPhong>(boxes, lights);
            break;
        case 1:
            examples->push_back<BenchmarkForwardPhongText>(boxes, lights);
            break;
        case 2:
            examples->push_back<BenchmarkDeferredPhong>(boxes, lights);
            break;
        case 3:
            examples->push_back<BenchmarkDeferredPhongText>(boxes, lights);
            break;
        case 4:
            examples->push_back<BenchmarkForwardPBR>(boxes, lights);
            break;
        case 5:
            examples->push_back<BenchmarkForwardPBRText>(boxes, lights);
            break;
        case 6:
            examples->push_back<BenchmarkDeferredPBR>(boxes, lights);
            break;
        case 7:
            examples->push_back<BenchmarkDeferredPBRText>(boxes, lights);
            break;
        default:
            UNREACHABLE();
        }
    } else {
        examples->push_back<DefaultTextShader>();
        examples->push_back<QuadTextShader>();
        examples->push_back<ForwardLit>();
        examples->push_back<DeferredShading>();
        examples->push_back<AlphaBlend>();
        examples->push_back<AlphaThreshold>();
        examples->push_back<Animation>();
        examples->push_back<NormalMap>();
        examples->push_back<PBR>();
        examples->push_back<DeferredPBR>();
        examples->push_back<CustomFunction>();
        examples->push_back<PerlinNoise>();
        examples->push_back<BenchmarkForwardPhong>(boxes, lights);
        examples->push_back<BenchmarkForwardPhongText>(boxes, lights);
        examples->push_back<BenchmarkDeferredPhong>(boxes, lights);
        examples->push_back<BenchmarkDeferredPhongText>(boxes, lights);
        examples->push_back<BenchmarkForwardPBR>(boxes, lights);
        examples->push_back<BenchmarkForwardPBRText>(boxes, lights);
        examples->push_back<BenchmarkDeferredPBR>(boxes, lights);
        examples->push_back<BenchmarkDeferredPBRText>(boxes, lights);
    }
}

void release_examples(com::Folder *root) {
    root->erase(Example::folder_name());
    root->erase("directional-light");
    gfx::material_system()->materials()->erase(Example::folder_name());
    gfx::shader_system()->shaders()->erase(Example::folder_name());
    gfx::buffer_system()->buffers()->erase(Example::folder_name());
    gfx::object_system()->objects()->erase(Example::folder_name());
    gfx::light_system()->lights()->erase(Example::folder_name());
}

} // namespace app
