#include <app/examples.hpp>
#include <app/index.hpp>
#include <app/presenter.hpp>
#include <gfx/index.hpp>
#include <osi/index.hpp>
#include <iostream>

namespace app {

Presenter::Presenter() : com::Runner{ self_name() } {
#ifdef MEASURING_FPS
    glCreateQueries(GL_TIME_ELAPSED, 1, &m_render_time_query);
#endif
}

Presenter::~Presenter() {
#ifdef MEASURING_FPS
    double fps_gpu = 1000. * static_cast<double>(m_cycles) / (static_cast<double>(m_total_time) * 1e-6f);
    std::cout << fps_gpu << std::endl;
#endif
}

static com::Folder *generate_grid() {
#ifdef MEASURING
    return nullptr;
#else
    auto shader = gfx::shader_system()->insert_shader<gfx::ShaderGraph>(
        {"presenter"}, "grid-shader", gfx::Shader::UNLIT);

    auto color = shader->insert<gfx::ShaderGraph::VaryingColorNode>();
    shader->connect(shader->root(), gfx::ShaderGraph::MasterNodeUnlit::color, color, 0);

    auto emission = shader->insert<gfx::ShaderGraph::ConstantNode>(vec3{0.});
    shader->connect(shader->root(), gfx::ShaderGraph::MasterNodeUnlit::emission, emission, 0);

    auto material = gfx::material_system()->insert_material(
        "grid-material", shader, { "presenter" });

    auto grid = gfx::object_system()->insert_object(
        { "grid" }, material, gfx::buffer_generators()->insert_procedural_grid());
    gfx::object_system()->push_frame_back(grid, grid_frame());

    return grid;
#endif
}

void Presenter::initialize() {
    osi::presenters()->push_back<com::Folder>("app")->push_back<com::Link>(self_name() + ".link", this);

    m_grid = generate_grid();
    m_examples_iterator = examples()->items().begin();
    m_active_example = dynamic_cast<Example *>(*m_examples_iterator);
    m_active_example->activate();

    // print_tree(*root());
    // print_tree(*gfx::material_system()->folder());
}

void Presenter::release() {
    osi::presenters()->find<com::Folder>("app")->erase(self_name() + ".link");
}

void Presenter::next_round() {
    if (m_warmup_cycles > 0) {
        m_active_example->next_round(m_grid);
        m_warmup_cycles--;
        return;
    }

#ifdef MEASURING_FPS
    glBeginQuery(GL_TIME_ELAPSED, m_render_time_query);
#endif

    m_active_example->next_round(m_grid);

#ifdef MEASURING_FPS
    glEndQuery(GL_TIME_ELAPSED);
    glFinish();
    GLuint64 render_time;
    glGetQueryObjectui64v(m_render_time_query, GL_QUERY_RESULT, &render_time);
    m_total_time += render_time;
    ++m_cycles;
#endif
}

void Presenter::next_example() {
    m_active_example->deactivate();

    if (++m_examples_iterator == examples()->items().end())
        m_examples_iterator = examples()->items().begin();

    m_active_example = dynamic_cast<Example *>(*m_examples_iterator);
    m_active_example->activate();
}

void Presenter::prev_example() {
    m_active_example->deactivate();

    if (m_examples_iterator == examples()->items().begin())
        m_examples_iterator = examples()->items().end();
    --m_examples_iterator;

    m_active_example = dynamic_cast<Example *>(*m_examples_iterator);
    m_active_example->activate();
}

} // namespace app
