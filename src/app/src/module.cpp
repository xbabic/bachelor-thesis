#include <app/module.hpp>
#include <app/updater.hpp>
#include <app/presenter.hpp>
#include <app/index.hpp>
#include <com/frame.hpp>

namespace app {

void boot(com::Folder* const ctx_root)
{
    com::Folder* const root_app = ctx_root->push_back<com::Folder>("app");
    initialize_examples(root_app);
    root_app->push_back<com::Folder>("camera")->push_back<com::Frame>();
    root_app->push_back<com::Folder>("grid")->push_back<com::Frame>();
    root_app->push_back<Updater>();
    root_app->push_back<Presenter>();
    app::index();
}

void shutdown(com::Folder* const ctx_root)
{
    com::Folder* const root_app = ctx_root->find<com::Folder>("app");
    root_app->erase(root_app->find<Presenter>(Presenter::self_name()));
    root_app->erase(root_app->find<Updater>(Updater::self_name()));
    root_app->erase(root_app->find<com::Folder>("grid"));
    root_app->erase(root_app->find<com::Folder>("camera"));
    release_examples(root_app);
    ctx_root->erase(root_app);
}

}
