#include <app/program_options.hpp>
#include <app/program_info.hpp>
#include <utils/assumptions.hpp>
#include <stdexcept>
#include <iostream>

namespace app {

program_options::program_options(int argc, char* argv[])
    : program_options_default(argc, argv)
{
    add_option(
        "scene",
        
        "A directory of a scene to be loaded. A scene directory always "
        "contains a file 'hierarchy.json'. The scene is  a relative "
        "path to the data root directory (see --data option).",
        
        "1"
        );
    add_option(
        "boxes",
        "Number of boxes to render in benchmarking examples.",
        "1");
    add_option(
        "lights",
        "Number of lights to render in benchmarking examples.",
        "1");
    add_option(
        "bench",
        "The index of the benchmark to run.",
        "1");
}

static program_options_ptr  global_program_options;

void initialise_program_options(int argc, char* argv[])
{
    ASSUMPTION(!global_program_options.operator bool());
    global_program_options = program_options_ptr(new program_options(argc,argv));
}

program_options_ptr get_program_options()
{
    ASSUMPTION(global_program_options.operator bool());
    return global_program_options;
}

std::ostream& operator<<(std::ostream& ostr, program_options_ptr options)
{
    ASSUMPTION(options.operator bool());
    options->operator<<(ostr);
    return ostr;
}

}
