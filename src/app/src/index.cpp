#include <app/index.hpp>
#include <app/examples.hpp>

namespace app {

Index const& Index::instance()
{
    static Index const idx;
    return idx;
}

Index::Index()
    : m_root { com::Folder::root()->find<com::Folder>("app") }
    , m_camera_frame{ m_root->find<com::Folder>("camera")->find<com::Frame>(com::Frame::self_file_name()) }
    , m_grid_frame{ m_root->find<com::Folder>("grid")->find<com::Frame>(com::Frame::self_file_name()) }
    , m_examples{ m_root->find<com::Folder>(Example::folder_name()) }
{}

}
