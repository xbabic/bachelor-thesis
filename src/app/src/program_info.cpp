#include <app/program_info.hpp>

namespace app {

std::string  get_program_name()
{
    return "app";
}

std::string  get_program_version()
{
    return "1.0.0";
}

std::string  get_program_description()
{
    return "The purpose of this project is to showcase the functionality created as a part of my bachelor's thesis.\n";
}

}
