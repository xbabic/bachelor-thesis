#include <osi/index.hpp>
#include <app/program_info.hpp>
#include <app/program_options.hpp>
#include <app/module.hpp>
#include <osi/run.hpp>
#include <utils/config.hpp>
#include <utils/timeprof.hpp>
#include <utils/log.hpp>
#include <filesystem>
#include <fstream>
#include <memory>
#include <stdexcept>
#include <iostream>
#if PLATFORM() == PLATFORM_WINDOWS()
#   pragma comment(linker, "/SUBSYSTEM:windows /ENTRY:mainCRTStartup") 
#endif

#if BUILD_RELEASE() == 1
static void save_crash_report(std::string const& crash_message)
{
    std::cout << "ERROR: " << crash_message << "\n";
    std::ofstream  ofile( app::get_program_name() + "_CRASH.txt", std::ios_base::app );
    ofile << crash_message << "\n";
}
#endif

int main(int argc, char* argv[])
{
#if BUILD_RELEASE() == 1
    try
#endif
    {
        LOG_INITIALISE(app::get_program_name(), LSL_WARNING);
        app::initialise_program_options(argc,argv);
        if (app::get_program_options()->helpMode()) {
            std::cout << app::get_program_options();
            return 0;
        }
        if (app::get_program_options()->versionMode()) {
            std::cout << app::get_program_version() << "\n";
            return 0;
        }

        osi::run({
            .window_title = "app",
            .resizable = true,
            .windowed = true,
            .min_dt = 0.f,
            .user_modules = {
                { app::boot, app::shutdown },
                }
            });
        TMPROF_PRINT_TO_FILE(app::get_program_name(),true);
    }
#if BUILD_RELEASE() == 1
    catch(std::exception const& e)
    {
        try { save_crash_report(e.what()); } catch (...) {}
        return -1;
    }
    catch(...)
    {
        try { save_crash_report("Unknown exception was thrown."); } catch (...) {}
        return -2;
    }
#endif

#ifdef MEASURING_FPS
    double fps = static_cast<double>(osi::timer()->passed_rounds()) / osi::timer()->passed_seconds();
    std::cout << fps << std::endl;
#endif
    return 0;
}
