#ifndef APP_INDEX_HPP_INCLUDED
#   define APP_INDEX_HPP_INCLUDED

#   include <com/context.hpp>
#   include <com/frame.hpp>

namespace app {

struct Index final
{
    static Index const& instance();

    com::Folder* root() const { return m_root; }
    com::Frame* camera_frame() const { return m_camera_frame; }
    com::Frame* grid_frame() const { return m_grid_frame; }
    com::Folder* examples() const { return m_examples; }

private:
    Index();
    Index(Index const&) = delete;
    Index(Index&&) = delete;
    Index& operator=(Index const&) const = delete;
    Index& operator=(Index&&) const = delete;

    com::Folder* m_root;
    com::Frame* m_camera_frame;
    com::Frame* m_grid_frame;
    com::Folder* m_examples;
};

inline Index const& index() { return Index::instance(); }

inline com::Folder* root() { return index().root(); }
inline com::Frame* camera_frame() { return index().camera_frame(); }
inline com::Frame* grid_frame() { return index().grid_frame(); }
inline com::Folder* examples() { return index().examples(); }

}

#endif
