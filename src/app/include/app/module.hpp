#ifndef APP_MODULE_HPP_INCLUDED
#   define APP_MODULE_HPP_INCLUDED

namespace com { struct Folder; }

namespace app {

void boot(com::Folder* ctx_root);
void shutdown(com::Folder* ctx_root);

}

#endif
