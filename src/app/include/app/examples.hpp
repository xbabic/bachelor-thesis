#ifndef APP_EXAMPLES_HPP_INCLUDED
#define APP_EXAMPLES_HPP_INCLUDED

#include <gfx/shader_graph.hpp>
#include <gfx/material.hpp>
#include <com/context.hpp>
#include <gfx/text_shader.hpp>
#include <string>

namespace app {

struct Example : public com::Folder {
    using Folder::Folder;

    static std::string folder_name() { return "examples"; }
    static std::string light_boxes_folder_name() { return "light-boxes"; }

    virtual void next_round(Folder *grid) = 0;
    virtual void activate();
    virtual void deactivate();

  protected:
    Folder *directional_light() const;
    static Folder *ambient_light();
    Folder *generate_point_light(const std::string &name, vec3 position, vec3 color,
                                 float atten_const, float atten_lin, float atten_quad);
    Folder *random_point_light();

    void add_random_frame(Folder *object);

    Folder *m_objects = nullptr;
};

struct DefaultTextShader final : public Example {
    DefaultTextShader() : Example("DefaultTextShader") {}

    void initialize() override;
    void next_round(Folder *grid) override;

  private:
    Folder *m_objects = nullptr;
};

struct QuadTextShader final : public Example {
    QuadTextShader() : Example("QuadTextShader") {}

    void initialize() override;
    void next_round(Folder *grid) override;
};

struct ForwardLit final : public Example {
    ForwardLit() : Example("ForwardLit") {}

    void initialize() override;
    void next_round(Folder *grid) override;
};

struct DeferredShading final : public Example {
    DeferredShading() : Example("DeferredShading") {}

    void initialize() override;
    void next_round(Folder *grid) override;

  private:
    std::vector<const Folder *> m_gathered_lights;
};

struct AlphaBlend final : public Example {
    AlphaBlend() : Example("AlphaBlend") {}

    void initialize() override;
    void next_round(Folder *grid) override;
};

struct AlphaThreshold final : public Example {
    AlphaThreshold() : Example("AlphaThreshold") {}

    void initialize() override;
    void next_round(Folder *grid) override;
    void activate() override;
    void deactivate() override;
};

struct Animation final : public Example {
    Animation() : Example("Animation") {}

    void initialize() override;
    void next_round(Folder *grid) override;
};

struct NormalMap final : public Example {
    NormalMap() : Example("NormalMap") {}

    void initialize() override;
    void next_round(Folder *grid) override;
};

struct PBR final : public Example {
    PBR() : Example("PBR") {}

    void initialize() override;
    void next_round(Folder *grid) override;
};

struct DeferredPBR final : public Example {
    DeferredPBR() : Example("DeferredPBR") {}

    void initialize() override;
    void next_round(Folder *grid) override;

private:
    std::vector<const Folder *> m_gathered_lights;
};

struct CustomFunction final : public Example {
    CustomFunction() : Example("CustomFunction") {}

    void initialize() override;
    void next_round(Folder *grid) override;
    void activate() override;
    void deactivate() override;
};

struct PerlinNoise final : public Example {
    PerlinNoise() : Example("PerlinNoise") {}

    void initialize() override;
    void next_round(Folder *grid) override;
    void activate() override;
    void deactivate() override;
};

struct Benchmark : public Example {
    Benchmark(const std::string &name, unsigned boxes, unsigned lights);

    void activate() override;

protected:
    unsigned m_boxes, m_lights;
    gfx::Material *m_material = nullptr;
};

struct BenchmarkForwardPhong final : public Benchmark {
    explicit BenchmarkForwardPhong(unsigned boxes, unsigned lights) : Benchmark("BenchmarkForwardPhong", boxes, lights) {}

    void initialize() override;
    void next_round(Folder *grid) override;
};

struct BenchmarkForwardPhongText final : public Benchmark {
    explicit BenchmarkForwardPhongText(unsigned boxes, unsigned lights) : Benchmark("BenchmarkForwardPhongText", boxes, lights) {}

    void initialize() override;
    void next_round(Folder *grid) override;

private:
    std::unique_ptr<gfx::ShaderGraph::TextureNode> diffuse, specular;
};

struct BenchmarkDeferredPhong final : public Benchmark {
    explicit BenchmarkDeferredPhong(unsigned boxes, unsigned lights) : Benchmark("BenchmarkDeferredPhong", boxes, lights) {}

    void initialize() override;
    void next_round(Folder *grid) override;

private:
    std::vector<const Folder *> m_gathered_lights;
};

struct BenchmarkDeferredPhongText final : public Benchmark {
    explicit BenchmarkDeferredPhongText(unsigned boxes, unsigned lights) : Benchmark("BenchmarkDeferredPhongText", boxes, lights) {}

    void initialize() override;
    void next_round(Folder *grid) override;

private:
    std::unique_ptr<gfx::ShaderGraph::TextureNode> diffuse, specular;
    std::vector<const Folder *> m_gathered_lights;
};

struct BenchmarkForwardPBR final : public Benchmark {
    explicit BenchmarkForwardPBR(unsigned boxes, unsigned lights) : Benchmark("BenchmarkForwardPBR", boxes, lights) {}

    void initialize() override;
    void next_round(Folder *grid) override;
};

struct BenchmarkForwardPBRText final : public Benchmark {
    explicit BenchmarkForwardPBRText(unsigned boxes, unsigned lights) : Benchmark("BenchmarkForwardPBRText", boxes, lights) {}

    void initialize() override;
    void next_round(Folder *grid) override;

private:
    std::vector<std::unique_ptr<gfx::ShaderGraph::TextureNode>> m_textures;
};

struct BenchmarkDeferredPBR final : public Benchmark {
    explicit BenchmarkDeferredPBR(unsigned boxes, unsigned lights) : Benchmark("BenchmarkDeferredPBR", boxes, lights) {}

    void initialize() override;
    void next_round(Folder *grid) override;

private:
    std::vector<const Folder *> m_gathered_lights;
};

struct BenchmarkDeferredPBRText final : public Benchmark {
    explicit BenchmarkDeferredPBRText(unsigned boxes, unsigned lights) : Benchmark("BenchmarkDeferredPBRText", boxes, lights) {}

    void initialize() override;
    void next_round(Folder *grid) override;

private:
    std::vector<std::unique_ptr<gfx::ShaderGraph::TextureNode>> m_textures;
    std::vector<const Folder *> m_gathered_lights;
};

void initialize_examples(com::Folder *root);
void release_examples(com::Folder *root);

} // namespace app

#endif