#ifndef APP_PROGRAM_INFO_HPP_INCLUDED
#   define APP_PROGRAM_INFO_HPP_INCLUDED

#   include <string>

namespace app {

std::string  get_program_name();
std::string  get_program_version();
std::string  get_program_description();

}

#endif
