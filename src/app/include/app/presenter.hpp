#ifndef APP_PRESENTER_HPP_INCLUDED
#   define APP_PRESENTER_HPP_INCLUDED

#   include <com/runner.hpp>
#   include <math/math.hpp>
#   include <app/examples.hpp>

namespace com { struct Frame; }

namespace app {

struct Presenter : public com::Runner
{
    static inline std::string self_name() { return "presenter.run"; }

    Presenter();
    ~Presenter() override;

    void next_round() override;

    void next_example();
    void prev_example();

protected:

    void initialize() override;
    void release() override;

private:
    com::Folder *m_grid = nullptr;
    std::list<ContextItem*>::const_iterator m_examples_iterator;
    Example *m_active_example = nullptr;
    GLuint m_render_time_query = 0;
    GLuint64 m_total_time = 0;
    GLuint64 m_cycles = 0;
    GLuint64 m_warmup_cycles = 10;
};

}

#endif
