#!/bin/bash

MEASUREMENTS_FILE="measurements"
APP="../cmake-build-release/src/app/app_Linux_Release"

if [ $# -ne 1 ]; then
    echo "expecting one argument: benchmark index"
    exit 1
fi

rm "$MEASUREMENTS_FILE" app--*.html 2> /dev/null

for BOXES in $(seq 100 100 500); do
    for LIGHTS in $(seq 100 100 500); do
        "$APP" --boxes "$BOXES" --lights "$LIGHTS" --bench "$@" >> "${MEASUREMENTS_FILE}";
    done
done

python generate-csv.py "$MEASUREMENTS_FILE"
