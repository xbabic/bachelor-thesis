# Measurements
This folder contains scripts used for automating
the measurements made in this thesis.

## Prerequisites
* Linux shell
* Python 3

## FPS
1. Uncomment lines #5 and #6 in `CMakelists.txt` that define the `MEASURING`
and `MEASURING_FPS` macros.
2. Build the release version of the app with `-DCMAKE_BUILD_TYPE=Release`
(consult root readme).
3. Edit the `measuring-utils/run-measuring.sh` file. Set
the `APP` variable to the path of the app executable.
4. Enter the `dist/` directory.
```bash
cd dist
```
5. Copy the measuring scripts to the `dist/` folder
```bash
cp ../measuring-utils/* .
```
6. Run the `run-measuring.sh` script with the index [0-7] of the benchmark to run.
This will create the `cpu.csv` and `gpu.csv` files.
```bash
bash run-measuring.sh <benchmark-index>
```

## Compile Time
1. Uncomment lines #5 and #7 in `CMakelists.txt` that define the `MEASURING`
and `MEASURING_COMPILE` macros.
2. Build the release version of the app with `-DCMAKE_BUILD_TYPE=Release`
(consult root readme).
3. Run the app inside the `dist/` directory. The results are printed to `stdout`.
