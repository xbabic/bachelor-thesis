import sys


def objects_range():
    return range(100, 501, 100)


def generate_csv(file):
    header = "boxes," + ','.join([str(lights) for lights in objects_range()]) + '\n'
    cpu, gpu = [header], [header]
    with open(file, "r") as f:
        cpu_line = False
        for boxes in objects_range():
            cpu_csv_line = str(boxes) + ','
            gpu_csv_line = str(boxes) + ','
            for lights in objects_range():
                for i in range(2):
                    fps = float(f.readline())
                    if cpu_line:
                        cpu_csv_line += str(fps) + ','
                    else:
                        gpu_csv_line += str(fps) + ','
                    cpu_line = not cpu_line
            cpu.append(cpu_csv_line[:-1] + '\n')
            gpu.append(gpu_csv_line[:-1] + '\n')

    with open("cpu.csv", "w") as f:
        f.writelines(cpu)
    with open("gpu.csv", "w") as f:
        f.writelines(gpu)


def main():
    if len(sys.argv) < 2:
        print("Required arguments: measurements-file")
        return

    generate_csv(sys.argv[1])


if __name__ == "__main__":
    main()
